
<p  align="center">

<a  href=""  rel="noopener">

<img  width=200px  height=200px  src="https://i.imgur.com/6wj0hh6.jpg"  alt="Project logo"></a>

</p>

  

<h3  align="center">TecMilenio Bank</h3>

  

<div  align="center">

  

[![Status](https://img.shields.io/badge/status-active-success.svg)]()

[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

  

</div>

  

---

  

<p  align="center"> Evidence 2 for the Programming fundamentals class

<br>

</p>

  

## 📝 Table of Contents

  

- [About](#about)

- [Getting Started](#getting_started)

- [Deployment](#deployment)

- [Usage](#usage)

- [Built Using](#built_using)

- [Authors](#authors)
  

## 🧐 About <a name = "about"></a>

  

Simple PoC of fundamental OOP

  

## 🏁 Getting Started <a name = "getting_started"></a>

  

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See [deployment](#deployment) for notes on how to deploy the project on a live system.

  

### Prerequisites

  

Docker and a free port :)

  

```

docker -v

Docker version 25.0.3, build 4debf41

```

  

### Installing

  

A step by step series of examples that tell you how to get a development env running.

  
  

```

sh start.sh

```

  

Example output

  

```

dubach@datascience-7900:~/Documents/tecmileniobank$ sh start.sh

[+] Stopping 1/0

✔ Container tecmileniobank-api-1 Stopped 0.0s

? Going to remove tecmileniobank-api-1 Yes

[+] Removing 1/0

✔ Container tecmileniobank-api-1 Removed 0.0s

[+] Building 1.5s (23/23) FINISHED docker:default

=> [api internal] load build definition from Dockerfile 0.0s

=> => transferring dockerfile: 293B 0.0s

=> [atm internal] load metadata for docker.io/library/alpine:3.14 0.9s

=> [atm internal] load metadata for docker.io/library/golang:1.19-alpine 1.0s

=> [api auth] library/golang:pull token for registry-1.docker.io 0.0s

=> [api auth] library/alpine:pull token for registry-1.docker.io 0.0s

=> [api internal] load .dockerignore 0.0s

=> => transferring context: 2B 0.0s

=> [atm development 1/5] FROM docker.io/library/golang:1.19-alpine@sha256:0ec0646e208ea58e5d29e558e39f2e59fccf39b7bda306cb53bbaff91919eca5 0.0s

=> [atm stage-1 1/3] FROM docker.io/library/alpine:3.14@sha256:0f2d5c38dd7a4f4f733e688e3a6733cb5ab1ac6e3cb4603a5dd564e5bfb80eed 0.0s

=> [api internal] load build context 0.0s

=> => transferring context: 111B 0.0s

=> CACHED [atm stage-1 2/3] WORKDIR /app 0.0s

=> CACHED [atm development 2/5] RUN apk add git gcc g++ linux-headers 0.0s

=> CACHED [atm development 3/5] WORKDIR /app 0.0s

=> CACHED [api development 4/5] COPY . /app 0.0s

=> CACHED [api development 5/5] RUN go mod download && go build -o main main.go 0.0s

=> CACHED [api stage-1 3/3] COPY --from=development /app/main . 0.0s

=> [api] exporting to image 0.0s

=> => exporting layers 0.0s

=> => writing image sha256:74be1048302ffb62f2197a31332a2812ee39dab11534f5e11aaf515442a72834 0.0s

=> => naming to docker.io/library/tecmileniobank-api 0.0s

=> [atm internal] load build definition from Dockerfile 0.0s

=> => transferring dockerfile: 293B 0.0s

=> [atm internal] load .dockerignore 0.0s

=> => transferring context: 2B 0.0s

=> [atm internal] load build context 0.0s

=> => transferring context: 274B 0.0s

=> CACHED [atm development 4/5] COPY . /app 0.0s

=> CACHED [atm development 5/5] RUN go mod download && go build -o main main.go 0.0s

=> CACHED [atm stage-1 3/3] COPY --from=development /app/main . 0.0s

=> [atm] exporting to image 0.0s

=> => exporting layers 0.0s

=> => writing image sha256:c4ae80f7636ef4c8510112924da3123880bdb15358cbe0b9d9a103c096444dc1 0.0s

=> => naming to docker.io/library/tecmileniobank-atm 0.0s

[+] Running 1/1

✔ Container tecmileniobank-api-1 Started 0.5s

[+] Creating 1/0

✔ Container tecmileniobank-api-1 Running 0.0s

Cuál es tú número de tarjeta:

```

  

## 🔧 Running the tests <a name = "tests"></a>

  

N/A probably won't have time to implement it

### Break down into end to end tests

  

Theres no database access, everything is hardcoded on api/main.go

  

```

dubach@datascience-7900:~/Documents/TecmilenioBank/api$ curl http://localhost:8080/api/tarjetas/

[

{

"id": 1,

"numero": "4242424242424240",

"cvc": "123",

"nip": "4240",

"expira": "12/24"

},

{

"id": 2,

"numero": "4000056655665550",

"cvc": "124",

"nip": "5550",

"expira": "12/23"

},

{

"id": 3,

"numero": "5555555555554440",

"cvc": "125",

"nip": "4440",

"expira": "11/24"

},

{

"id": 4,

"numero": "2223003122003220",

"cvc": "126",

"nip": "3220",

"expira": "2/24"

}

]

```

  

### And coding style tests

```

4242424242424240 is a valid card

4000056655665550 is a expired card

```

  

## 🎈 Usage <a name="usage"></a>

  

Add notes about how to use the system.

  

Just running the start.sh will delete any pre existing projects, build and deploy the api and console client.

  

## 🚀 Deployment <a name = "deployment"></a>

  

clone the repo, and run start.sh

  

## ⛏️ Built Using <a name = "built_using"></a>

  
  

- [Gin](https://github.com/gin-gonic/gin) - Server Framework

- [Go] (https://go.dev) - CLI App

  

## ✍️ Authors <a name = "authors"></a>

  

- [@diegoubach](https://gitlab.com/diegoubach)