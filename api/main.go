package main

// This is a simple API that returns information about clients, cards and accounts
import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// Define the structs for the data, very simple and not very realistic but it's just for the homework
type Cliente struct {
	ID       int    `json:"id"`
	Nombre   string `json:"nombre"`
	Apellido string `json:"apellido"`
}

type Tarjeta struct {
	ID     int    `json:"id"`
	Numero string `json:"numero"`
	Cvc    string `json:"cvc"`
	Nip    string `json:"nip"`
	Expira string `json:"expira"`
}

type Cuenta struct {
	ID        int     `json:"id"`
	Numero    string  `json:"numero"`
	Saldo     float64 `json:"saldo"`
	ClienteID int     `json:"cliente_id"`
	TarjetaID int     `json:"tarjeta_id"`
}

// I'm not using a database so I'm using slices of structs as a very simple way to simulate a database
var clientes = []Cliente{
	{ID: 1, Nombre: "Juan", Apellido: "Montes"},
	{ID: 2, Nombre: "Pedro", Apellido: "Zapata"},
	{ID: 3, Nombre: "Ana", Apellido: "Martínez"},
	{ID: 4, Nombre: "Rogelio", Apellido: "Guerra"},
}

var tarjetas = []Tarjeta{
	{ID: 1, Numero: "4242424242424240", Cvc: "123", Nip: "4240", Expira: "12/24"},
	{ID: 2, Numero: "4000056655665550", Cvc: "124", Nip: "5550", Expira: "12/23"},
	{ID: 3, Numero: "5555555555554440", Cvc: "125", Nip: "4440", Expira: "11/24"},
	{ID: 4, Numero: "2223003122003220", Cvc: "126", Nip: "3220", Expira: "2/24"},
}
var cuentas = []Cuenta{
	{ID: 1, Numero: "123456789012", Saldo: 15000, ClienteID: 1, TarjetaID: 1},
	{ID: 2, Numero: "123456789013", Saldo: 2000, ClienteID: 2, TarjetaID: 2},
	{ID: 3, Numero: "123456789014", Saldo: 35000, ClienteID: 3, TarjetaID: 3},
	{ID: 4, Numero: "123456789015", Saldo: 800, ClienteID: 4, TarjetaID: 4},
}

func getCliente(c *gin.Context, id int) {
	// this is a horrible hack but as I'm not using a database I'll do it
	found := false
	for _, cliente := range clientes {
		if cliente.ID == id {
			c.IndentedJSON(http.StatusOK, cliente)
			found = true
			break
		}
	}
	if !found {
		c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
	}

}

func main() {
	fmt.Println("Starting Server")
	router := gin.Default()
	router.GET("/api/clientes/:id", func(c *gin.Context) {
		id := c.Param("id")
		idInt, _ := strconv.Atoi(id) // Convert id from string to int
		getCliente(c, idInt)
	})

	router.GET("/api/clientes/", func(c *gin.Context) {
		c.IndentedJSON(http.StatusOK, clientes)
	})

	router.GET("/api/cliente/:nombre/:apellido", func(c *gin.Context) {
		nombre := c.Param("nombre")
		apellido := c.Param("apellido")
		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, cliente := range clientes {
			if cliente.Nombre == nombre && cliente.Apellido == apellido {
				c.IndentedJSON(http.StatusOK, cliente)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/tarjetas/:id", func(c *gin.Context) {
		id := c.Param("id")
		idInt, _ := strconv.Atoi(id) // Convert id from string to int
		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, tarjeta := range tarjetas {
			if tarjeta.ID == idInt {
				c.IndentedJSON(http.StatusOK, tarjeta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/tarjetas/", func(c *gin.Context) {
		c.IndentedJSON(http.StatusOK, tarjetas)
	})

	router.GET("/api/tarjeta/:numero", func(c *gin.Context) {
		numero := c.Param("numero")
		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, tarjeta := range tarjetas {
			if tarjeta.Numero == numero {
				c.IndentedJSON(http.StatusOK, tarjeta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/cuentas/:id", func(c *gin.Context) {
		id := c.Param("id")
		idInt, _ := strconv.Atoi(id) // Convert id from string to int
		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, cuenta := range cuentas {
			if cuenta.ID == idInt {
				c.IndentedJSON(http.StatusOK, cuenta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/cuentas/", func(c *gin.Context) {
		c.IndentedJSON(http.StatusOK, cuentas)
	})

	router.GET("/api/cuenta/:numero", func(c *gin.Context) {
		numero := c.Param("numero")
		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, cuenta := range cuentas {
			if cuenta.Numero == numero {
				c.IndentedJSON(http.StatusOK, cuenta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/cuenta/tarjeta/:id", func(c *gin.Context) {
		id := c.Param("id")
		idInt, _ := strconv.Atoi(id) // Convert id from string to int

		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, cuenta := range cuentas {
			if cuenta.TarjetaID == idInt {
				c.IndentedJSON(http.StatusOK, cuenta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.GET("/api/cuenta/cliente/:id", func(c *gin.Context) {
		id := c.Param("id")
		idInt, _ := strconv.Atoi(id) // Convert id from string to int

		// this is a horrible hack but as I'm not using a database I'll do it
		found := false
		for _, cuenta := range cuentas {
			if cuenta.ClienteID == idInt {
				c.IndentedJSON(http.StatusOK, cuenta)
				found = true
				break
			}
		}
		if !found {
			c.IndentedJSON(http.StatusNotFound, gin.H{"message": "Cliente no encontrado"})
		}
	})

	router.Run(":8080")

}
