package account

import (
	"atm/card"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/google/uuid"
)

type Cuenta struct {
	ID        int     `json:"id"`
	Numero    string  `json:"numero"`
	Saldo     float64 `json:"saldo"`
	ClienteID int     `json:"cliente_id"`
	TarjetaID int     `json:"tarjeta_id"`
}

var apiUrl string = os.Getenv("API_URL")

func AccountByCardNumber(cardNumber string) Cuenta {
	// this can be improved but I'm not using a database so ¯\_(ツ)_/¯
	tarjetaId := card.CardIdByNumber(cardNumber)
	url := fmt.Sprintf("%s/api/cuenta/tarjeta/%d", apiUrl, tarjetaId)

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return Cuenta{}
	}
	defer response.Body.Close()
	if response.StatusCode == http.StatusNotFound {
		fmt.Println("Error: Unknown error. Please try again.")
		return Cuenta{}
	} else if response.StatusCode != http.StatusOK {
		fmt.Println("Unexpected status code:", response.StatusCode)
		return Cuenta{}
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return Cuenta{}
	}

	var cuenta Cuenta
	err = json.Unmarshal(body, &cuenta)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return Cuenta{}
	}

	return cuenta
}

func Depositar(cuenta *Cuenta, monto float64) {
	cuenta.Saldo += monto
}

func Retirar(cuenta *Cuenta, monto float64) {
	cuenta.Saldo -= monto
}

func Transferir(cuenta *Cuenta, externa string, monto float64) bool {
	transfer := AccountByCardNumber(externa)
	fmt.Println("Cuenta válida, transacción en proceso...")
	if transfer == (Cuenta{}) {
		fmt.Println("Cuenta externa no encontrada.")
		return false
	}
	cuenta.Saldo -= monto
	return true
}

func Saldo(cuenta *Cuenta) float64 {
	var imprimirSaldo string
	//Preguntar si quiere imprimir su saldo
	fmt.Println("¿Desea imprimir su saldo? (s/n)")
	_, err := fmt.Scanln(&imprimirSaldo)
	if err != nil {
		fmt.Println("Error reading input:", err)

	}
	// solo imprimir si la respuesta es "s"
	if imprimirSaldo == "s" {
		imprimirArchivoSaldo(cuenta)
	}
	return cuenta.Saldo
}

func imprimirArchivoSaldo(cuenta *Cuenta) {
	// obtener un uuid para no sobreescribir el archivo
	uuid := uuid.New().String()

	// crear el archivo
	file, err := os.Create("/app/data/saldo-" + uuid + ".txt")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer file.Close()

	file.WriteString(fmt.Sprintf("Saldo: %.2f\n", cuenta.Saldo))
}
