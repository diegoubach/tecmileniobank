package card

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var apiUrl string = os.Getenv("API_URL")

type Tarjeta struct {
	ID     int    `json:"id"`
	Numero string `json:"numero"`
	Cvc    string `json:"cvc"`
	Nip    string `json:"nip"`
	Expira string `json:"expira"`
}

func CardIdByNumber(cardNumber string) int {
	// Concatenate the card number with the URL string

	url := fmt.Sprintf("%s/api/tarjeta/%s", apiUrl, cardNumber)

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return 0
	}
	defer response.Body.Close()
	// Check the status code, if card not found return false and print generic error message to prevent information leakage
	if response.StatusCode == http.StatusNotFound {
		fmt.Println("Error: Unknown error. Please try again.")
		return 0
	} else if response.StatusCode != http.StatusOK {
		fmt.Println("Unexpected status code:", response.StatusCode)
		return 0
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return 0
	}

	// Parse JSON into struct
	var tarjeta Tarjeta
	err = json.Unmarshal(body, &tarjeta)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return 0
	}

	return tarjeta.ID
}

func ValidCardNumber(cardNumber string) bool {

	// Concatenate the card number with the URL string
	url := fmt.Sprintf("%s/api/tarjeta/%s", apiUrl, cardNumber)

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return false
	}
	defer response.Body.Close()
	// Check the status code, if card not found return false and print generic error message to prevent information leakage
	if response.StatusCode == http.StatusNotFound {
		fmt.Println("Error: Unknown error. Please try again.")
		return false
	} else if response.StatusCode != http.StatusOK {
		fmt.Println("Unexpected status code:", response.StatusCode)
		return false
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return false
	}

	// Parse JSON into struct
	var tarjeta Tarjeta
	err = json.Unmarshal(body, &tarjeta)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return false
	}

	// print tarjeta info

	parts := strings.Split(tarjeta.Expira, "/")
	if len(parts) != 2 {
		fmt.Println("Invalid expiry date format")
		return false
	}

	// Parse month and year to integers
	month, err := strconv.Atoi(parts[0])
	if err != nil {
		fmt.Println("Error parsing month:", err)
		return false
	}

	year, err := strconv.Atoi(parts[1])
	if err != nil {
		fmt.Println("Error parsing year:", err)
		return false
	}

	if !DateValid(month, year) {
		fmt.Println("Su plástico ha vencido, pase a ventanilla para obtener uno nuevo.")
		return false
	}

	return true
}

func DateValid(month, year int) bool {
	// Get the current month and year
	currentMonth := int(time.Now().Month())
	currentYear := time.Now().Year() - 2000

	// Check if the year is in the future
	if year > currentYear {
		return true
	}
	// Check if the year is the same and the month is in the future
	if year == currentYear && month > currentMonth {
		return true
	}

	return false
}

func ValidNip(cardNumber string, nip string) bool {
	// Concatenate the card number with the URL string

	url := fmt.Sprintf("%s/api/tarjeta/%s", apiUrl, cardNumber)

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return false
	}
	defer response.Body.Close()
	// Check the status code, if card not found return false and print generic error message to prevent information leakage
	if response.StatusCode == http.StatusNotFound {
		fmt.Println("Error: Unknown error. Please try again.")
		return false
	} else if response.StatusCode != http.StatusOK {
		fmt.Println("Unexpected status code:", response.StatusCode)
		return false
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return false
	}

	// Parse JSON into struct
	var tarjeta Tarjeta
	err = json.Unmarshal(body, &tarjeta)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return false
	}

	if tarjeta.Nip != nip {
		fmt.Println("Invalid NIP")
		return false
	}

	return true
}
