package client

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

type Cliente struct {
	ID       int    `json:"id"`
	Nombre   string `json:"nombre"`
	Apellido string `json:"apellido"`
}

var apiUrl string = os.Getenv("API_URL")

// ClientById returns a client by its ID
func ClientById(id int) Cliente {
	// this is a horrible hack but as I'm not using a database I'll do it
	// Concatenate the card number with the URL string

	url := fmt.Sprintf("%s/api/clientes/%d", apiUrl, id)
	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error:", err)
		return Cliente{}
	}
	defer response.Body.Close()
	if response.StatusCode == http.StatusNotFound {
		fmt.Println("Error: Unknown error. Please try again.")
		return Cliente{}
	} else if response.StatusCode != http.StatusOK {
		fmt.Println("Unexpected status code:", response.StatusCode)
		return Cliente{}
	}

	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println("Error:", err)
		return Cliente{}
	}

	var cliente Cliente
	err = json.Unmarshal(body, &cliente)
	if err != nil {
		fmt.Println("Error parsing JSON:", err)
		return Cliente{}
	}

	return cliente
}
