package main

// This is a simple ATM mockup for evidence 2 of TecMilenio programming fundamentals course
import (
	"atm/account"
	"atm/card"
	"atm/client"
	"fmt"
	"math"
)

func main() {
	// not using authentication tokens either, so I'm just calling the card and NIP functions directly and store a var for authentication
	var authenticated bool = false
	var valid bool
	var cardNumber string
	var cuenta account.Cuenta
	var cliente client.Cliente
	for {

		if !authenticated {
			valid, cardNumber = cardAndNipLoop()
			if !valid {
				break
			}
			authenticated = true
		}

		selection := showMenu()
		// making sure cuenta and client are not empty (might happen on the first run)
		if cuenta.Numero == "" {
			cuenta = account.AccountByCardNumber(cardNumber)
		}

		if cliente.Nombre == "" {
			cliente = client.ClientById(cuenta.ClienteID)
		}

		switch selection {
		case 1:
			// now that I have the account I can personalize the message
			message := fmt.Sprintf("Hola: %s %s, tu saldo es: %.2f", cliente.Nombre, cliente.Apellido, account.Saldo(&cuenta))
			fmt.Println(message)
		case 2:
			fmt.Print("Monto a depositar: ")
			var monto float64
			_, err := fmt.Scanln(&monto)
			if err != nil {
				fmt.Println("Error reading input:", err)
				continue
			}
			if math.Mod(monto, 50) != 0 {
				fmt.Println("Solo puedes depositar múltiplos de 50.")
				continue
			}
			account.Depositar(&cuenta, monto)
			fmt.Println(cuenta.Saldo)
		case 3:
			fmt.Print("Monto a retirar: ")
			var monto float64
			_, err := fmt.Scanln(&monto)
			if err != nil {
				fmt.Println("Error reading input:", err)
				continue
			}
			if !checkAmmount(&cuenta, monto) {
				fmt.Println("Fondos insuficientes.")
				continue
			}
			if math.Mod(monto, 100) != 0 {
				fmt.Println("Solo puedes retirar múltiplos de 100.")
				continue
			}
			account.Retirar(&cuenta, monto)
			fmt.Println(cuenta.Saldo)
		case 4:
			fmt.Println("Número de tarjeta a transferir: ")
			var externa string
			_, err := fmt.Scanln(&externa)
			if err != nil {
				fmt.Println("Error reading input:", err)
				continue
			}
			fmt.Println("Monto a transferir: ")
			var monto float64
			_, err = fmt.Scanln(&monto)
			if err != nil {
				fmt.Println("Error reading input:", err)
				continue
			}
			if !checkAmmount(&cuenta, monto) {
				fmt.Println("Fondos insuficientes.")
				continue
			}
			if account.Transferir(&cuenta, externa, monto) {
				fmt.Println("Transferencia exitosa.")
				message := fmt.Sprintf("Hola: %s %s, tu saldo es: %.2f", cliente.Nombre, cliente.Apellido, account.Saldo(&cuenta))
				fmt.Println(message)
			}
		case 5:
			fmt.Println("Gracias por usar TecMilenio ATM.")
			return

		}

	}
}

func cardAndNipLoop() (bool, string) {
	var cardNumber string
	var pin string
	// limit to 3 tries before exiting
	var tries int = 0
	// Loop until a valid card number is entered
	for {
		fmt.Print("Cuál es tú número de tarjeta: ")
		_, err := fmt.Scanln(&cardNumber)
		if err != nil {
			fmt.Println("Error reading input:", err)
			continue
		}

		if card.ValidCardNumber(cardNumber) {
			tries = 0
			break // Exit the loop if the card number is valid
		} else {
			tries++
			if tries >= 3 {
				fmt.Println("Demasiados intentos. Saliendo.")
				return false, cardNumber
			}
			fmt.Println("Tarjeta inválida, por favor intenta de nuevo.")
		}
	}

	// At this point, you have a valid card number request the PIN
	for {
		fmt.Print("Ingresa PIN: ")
		// TODO: Mask the input
		_, err := fmt.Scanln(&pin)
		if err != nil {
			fmt.Println("Error reading input:", err)
			continue
		}

		if card.ValidNip(cardNumber, pin) {
			break // Exit the loop if the card number is valid
		} else {
			tries++
			if tries >= 3 {
				fmt.Println("Demasiados intentos. Saliendo.")
				return false, cardNumber
			}
			fmt.Println("PIN inválido, por favor intenta de nuevo")
		}
	}

	// At this point, you have a valid card number and pin
	fmt.Println("Autenticado correctamente. Bienvenido.")
	return true, cardNumber
}

func showMenu() int {
	fmt.Println("1. Consulta de Saldo")
	fmt.Println("2. Depositar")
	fmt.Println("3. Retirar")
	fmt.Println("4. Transferir")
	fmt.Println("5. Salir")

	var option int
	for {
		fmt.Print("Ingresa opción: ")
		_, err := fmt.Scanln(&option)
		if err != nil {
			fmt.Println("Error:", err)
			continue
		}
		if option < 1 || option > 5 {
			fmt.Println("Opción inválida, por favor intenta de nuevo.")
		} else {
			break
		}
	}
	return option
}

// function to check ammount not greater than balance
func checkAmmount(cuenta *account.Cuenta, monto float64) bool {
	return monto <= cuenta.Saldo
}

// Path: atm/main.go
