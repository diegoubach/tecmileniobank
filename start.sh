#!/bin/sh

docker compose stop
docker compose rm 
docker rmi tecmileniobank-atm tecmileniobank-api
docker compose build

docker compose up -d api
docker compose run atm
